import React from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { createBoard, deleteBoard } from '../../store/actions/board.actions';
import { onUpdateUser } from '../../store/actions/user.actions';
import { BoardAdd } from './board-add'
import { setCardPopover } from '../../store/actions/system.actions';

import IconButton from '@mui/material/IconButton';
// import DeleteIcon from '@mui/icons-material/Delete';
import ClearOutlinedIcon from '@mui/icons-material/ClearOutlined';



// import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';







class _BoardPreview extends React.Component {
  state = {
    isModal: false,
    isOpen: false
  };


  onStar = async ev => {
    ev.preventDefault();
    const { user } = this.props;
    const { isStarred } = this.props;
    let updatedUser = { ...user };
    const { starredBoardsIds } = user;
    if (isStarred) {
      updatedUser = {
        ...updatedUser,
        starredBoardsIds: starredBoardsIds.filter(id => id !== this.props.board._id),
      };
    } else {
      updatedUser = {
        ...updatedUser,
        starredBoardsIds: [...starredBoardsIds, this.props.board._id],
      };
    }
    this.props.onUpdateUser(updatedUser);
  };

  onNew = () => {
    this.setState({ isModal: !this.state.isModal });
  };


  handleClose = () => {
    this.setState({ isOpen: false });
  };



  handleOpen = () => {
    this.setState({ isOpen: true });
    // setOpen(false);
  };

  onOpenPopover = (ev, props) => {
    let { name } = ev.target;
    // console.log(name)
    if (!name) {
      name = ev.currentTarget.name
    }


    this.props.setCardPopover(name, ev.target, props);
  };





  render() {
    const { isAdd, isStarred, board } = this.props;
    const imgUrl = board?.style.imgUrl ? board.style.imgUrl : '';
    const bgColor = board?.style.bgColor ? board.style.bgColor : 'inherit';
    if (isAdd) {
      return (
        <>
          <button
            onClick={this.onNew}
            className="board-preview new-board"
            style={!isAdd ? { backgroundColor: '#f0f2f4' } : {}}>
            <span></span>
            <div>Create new board</div>
            { }
          </button>
          <BoardAdd isModal={this.state.isModal} onClose={this.onNew} />
        </>
      );
    } else
      return (
        <div className='prv'>
          <Link
            to={`/board/${board._id}`}
            className={(bgColor !== 'inherit' ? 'bg-color' : '') + ' board-preview'}
            style={{ backgroundImage: `url(${imgUrl})`, backgroundColor: bgColor }}>
            <span></span>
            <div>
              {board.title}
              {isStarred === 'true' && <span>{board.title}</span>}
            </div>
            <span
              className={`${isStarred ? 'starred' : ''} star`}
              onClick={ev => this.onStar(ev)}></span>
            
          
            
          </Link>
         
         {this.props.isOwner && <IconButton className="comment-delete-button" aria-label="delete"
              name="remove-item"
              onClick={ev =>

                // console.log(ev.currentTarget.name)
                this.handleOpen()
              }

              style={{ position: "absolute", top: 1, right: 1, color: "white" }}>

              <ClearOutlinedIcon />
            </IconButton>
         }
          {this.props.isOwner && <Dialog
            open={this.state.isOpen}
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              {"are you sure you want to delete this board ?"}
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                are you sure you want to delete this board ?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose}>No</Button>
              <Button onClick={async () => {
                this.props.deleteBoard(board._id);
                this.handleClose()
              }

              } autoFocus>
                Yes
              </Button>
            </DialogActions>
          </Dialog>
          }        </div>
      );
  }
}

const mapDispatchToProps = {
  createBoard,
  onUpdateUser, setCardPopover, deleteBoard
};

const mapStateToProps = state => {
  return {
    boards: state.boardModule.boards,
    user: state.userModule.user,
  };
};

export const BoardPreview = connect(mapStateToProps, mapDispatchToProps)(_BoardPreview);